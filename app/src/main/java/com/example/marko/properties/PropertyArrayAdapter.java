package com.example.marko.properties;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marko on 11/1/16.
 */

public class PropertyArrayAdapter extends ArrayAdapter{

    private Context context;
    private List<Property> rentalProperties;

    //created when called
    public PropertyArrayAdapter(Context context, int resource, ArrayList<Property> objects) {
        super(context, resource, objects);

        this.context =  context;
        this.rentalProperties = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String fullAddress, finalPrice, finalBeds, finalBaths, finalCars;

        //get the property we are displaying
        Property property = rentalProperties.get(position);

        //get inflater and inflate xml layout for each item
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.property_layout,null);

        //assign views to respective variables for use
        ImageView image = (ImageView) view.findViewById(R.id.propertyImage);
        TextView address = (TextView) view.findViewById(R.id.propertyAddress);
        TextView description = (TextView) view.findViewById(R.id.propertyDescription);
        TextView price = (TextView) view.findViewById(R.id.propertyPrice);
        TextView bedrooms = (TextView) view.findViewById(R.id.propertyBedroom);
        TextView bathrooms = (TextView) view.findViewById(R.id.propertyBathrooms);
        TextView carspots = (TextView) view.findViewById(R.id.propertyCarspots);

        fullAddress = property.getStreetNumber() + " " + property.getStreetName() + ", " + property.getSuburb() + ", " + property.getState();
        address.setText(fullAddress);

        finalPrice = "$" + String.valueOf(property.getPrice());
        finalBeds = "Bed: " + String.valueOf(property.getBedrooms());
        finalBaths = "Bath: " + String.valueOf(property.getBathrooms());
        finalCars = "Car: " + String.valueOf(property.getCarSpots());

        description.setText(property.getDescription());
        price.setText(finalPrice);
        bedrooms.setText(finalBeds);
        bathrooms.setText(finalBaths);
        carspots.setText(finalCars);

        int imageID = context.getResources().getIdentifier(property.getImage(), "drawable", context.getPackageName());
        image.setImageResource(imageID);

        return view;
    }
}
