package com.example.marko.properties;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList <Property> rentalProperties = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create actual property elements
        rentalProperties.add(new Property(10, "Smith Street", "Sydney", "NSW", "A large 3 bedroom apartment right in the heart of Sydney! A rare find, with 3 bedrooms and a secured car park.","property_image_1", 450.00, 3, 1, 1));
        rentalProperties.add(new Property(66, "King Street", "Sydney", "NSW", "A fully furnished studio apartment overlooking the harbour. Minutes from the CBD and next to transport, this is a perfect set-up for city living.","property_image_2", 320.00, 1, 1, 1));
        rentalProperties.add(new Property(1, "Liverpool Road", "Liverpool", "NSW", "A standard 3 bedroom house in the suburbs. With room for several cars and right next to shops this is perfect for new families.", "property_image_3", 360.00, 3, 2, 2));
        rentalProperties.add(new Property(567, "Sunny Street", "Gold Coast", "QLD", "Come and see this amazing studio appartment in the heart of the gold coast, featuring stunning waterfront views.","property_image_4", 360.00 , 1, 1, 1));

        ArrayAdapter <Property> adapter = new PropertyArrayAdapter(this, 0, rentalProperties);

        ListView listView = (ListView) findViewById(R.id.customListView);
        listView.setAdapter(adapter);
    }
}
