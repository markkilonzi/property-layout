package com.example.marko.properties;

/**
 * Created by marko on 10/31/16.
 * Property class to set and get all the property elements
 */

public class Property {

        private int streetNumber;
        private String streetName;
        private String suburb;
        private String description;
        private String state;
        private String image;
        private Double price;
        private int bedrooms;
        private int bathrooms;
        private int carSpots;

    public Property(int streetNumber, String streetName, String suburb, String state, String description, String image, Double price, int bathrooms, int bedrooms, int carspots){

        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.suburb = suburb;
        this.description = description;
        this.state = state;
        this.image = image;
        this.price = price;
        this.bedrooms = bedrooms;
        this.bathrooms = bathrooms;
        this.carSpots = carspots;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public int getCarSpots() {
        return carSpots;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public Double getPrice() {
        return price;
    }

    public String getState() {
        return state;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String getSuburb() {
        return suburb;
    }
}
